# Application Readme

This file serves as a template for new applications' README. Copy this file
to the `README.md` file at the root of the application repository and customize it to taste.

## Description

This section should describe the application, its goals and its technological stack.

Here are some examples.

This application provides *foobar* functionalities through a RESTful API.
It also writes events to a Kafka message bus. It uses the user-management service to pull
user profile information.

### Technology used:
- Elixir 1.10
- [Phoenix Framework](https://www.phoenixframework.org/)
- PostgreSQL
- Kafka

## Build

This section should describe how to build the application from source code. It should be a step by step 
guide, going from cloning the repo to the finished artifact. 
It should also list all the required tools that a developer must install in older to build 
the application. If the application can be run locally, a `docker-compose.yml` file should be provided
in order to spin up the external dependencies, such as databases or message queues. 

Be sure to specify any other service the application may need as a dependency, internal or otherwise. This includes
SMTP senders, other L.I.F.E. applications, third party APIs, etc.


#### Requirements

Elixir 1.10 and Mix. To run it, PostgreSQL and Kafka are required. They can
be started using the included `docker-compose.yml` by running `docker-compose up -d` from the source root.

It also needs user-management to be available on `localhost:8083`. [Kubernetes port forwarding capabilities](https://kubectl.docs.kubernetes.io/pages/container_debugging/port_forward_to_pods.html)
can be used to locally access services running in the development environment.


#### Clone the repo

`git clone git@bitbucket.com:lifecorporation/app-repo.git`

#### Download dependencies

`mix deps.get`

#### Run locally

`mix phx.server`

#### Build for production

```bash
export MIX_ENV=prod
mkdir -p priv/static
mix do phx.digest, release
```

## Docker image

Instructions on how to build the Docker image. This SHOULD NOT require any other step other than cloning the
repository and running `docker build -t image-name:tag .`. Dockerfiles must be self contained and include all the step
necessary to build a complete release image (downloading dependencies, precompiling assets, etc).
Usage of [multi-stage docker builds](https://docs.docker.com/develop/develop-images/multistage-build/) is strongly recommended.

#### Clone the repo

`git clone git@bitbucket.com:lifecorporation/app-repo.git`

#### Build the image

`docker build -t lifecorporation/app-name:latest .`

## Lint and Test

This section should highlight how to run automated tests (unit, integration and/or end-to-end if available) against a build.
If a style guide enforcer or other linting tool is used, it should also detail how to run that.

#### Run unit tests

`mix test`

#### Run the linter

`mix format --check-formatted && mix credo`

## Configuration

This section should include a complete reference of all the knobs and tweaks that can or must be set
to configure the application based on the running environment, including any default value. Optional values MUST provide a documented default value.

```.env
# Change the application port
PORT=4000

# Set the log level
LOG_LEVEL=info

# Specify database credentials

## Database host
POSTGRES_HOST=localhost

## Database username
POSTGRES_USER=app

## Database password
POSTGRES_PASSWORD=password

# Sentry config

## Sentry DSN (required)
SENTRY_DSN

## Sentry environment
SENTRY_ENVIRONMENT=production
```

## Observability

This section should detail all the observability features
supported by the application. A support matrix can be detailed with the following template.

| Feature       | Supported | Notes                                             |
|---------------|-----------|---------------------------------------------------|
| JSON logging  | yes       | request_id integration is still under development |
| Health probes | yes       | Health check available at service:4000/health     |
| Sentry        | no        | N/A                                               |
| Prometheus    | yes       | Metrics available at service:4000/metrics         |
| OpenTelemetry | yes       | Only Jaeger supported as exporter                 |

## License

Copyright <*current year*> L.I.F.E Italia S.r.l, all rights reserved

