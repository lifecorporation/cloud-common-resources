# L.I.F.E. Cloud Common Resources

This repository contains templates, starter kits and documentation regarding the Cloud platform as a whole.

## Content

- *drone* contains example `.drone.yml` files for different languages and tech stacks
- *markdown* contains example `.md` files, such as READMEs and other documentation boilerplates
- *schemas* contains various [JSON Schemas]() definitions
